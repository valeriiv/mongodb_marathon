db.getCollection('courses').find({}).forEach(function(vel){
    if(vel.Value.length > 0) {
    var cost = vel.Value.replace(/,/g, '.');
    var tempVal = parseFloat(cost).toFixed(4);
    vel.Value = parseFloat(tempVal);
    db.courses.save(vel);
        }
})

db.getCollection('courses').aggregate([{
$project: { date: { $dateFromString: {
            dateString: "$Date"
            }
        }, Id: 1, rate: "$Value", Name: 1
    }
            },
{ $sort: {date: 1} },
{$out: "rates"}
])

db.getCollection('rates').createIndex({date: 1.0})

db.getCollection('rates').createIndex({rate: 1.0})

db.getCollection('rates').getIndexes()

db.getCollection('rates').find({date: "2013-02-06"}).explain()

db.getCollection('rates').find({rate: 30.6381}).explain()

db.getCollection('rates').find({rate: 30.6381, date: "2013-03-02"}).explain()