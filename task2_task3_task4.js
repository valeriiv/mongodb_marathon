const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');

const url = 'mongodb://localhost:27017';

const dbName = 'mongodbmarathon';

MongoClient.connect(url, function(err, client) {
    assert.equal(null, err);

    const db = client.db(dbName);
    const rates = db.collection('rates');
    const transactions = db.collection('transactions');
    const marathon = db.collection('marathon');
    const cashflowUsd = db.collection('cashflow');

    workWith3Collections(marathon, rates, transactions, cashflowUsd).then(res => {
        client.close();
    });

});

async function workWith3Collections(marathonCollection, ratesCollection, transactionsCollection, cashflowUsdCollection) {
    var sevenDays = 7;
    var dropTransactions = await transactionsCollection.drop();
    var dropCashflowUsd = await cashflowUsdCollection.drop();
    var totalRates = await marathonCollection.find({}).toArray();
    var dateTrans = await ratesCollection.find({}).sort({ "date": 1 }).toArray();
    var randDate;
    var datesWithWeekends = [];
    datesWithWeekends[0] = dateTrans[0];
    for (var n = 1; n < dateTrans.length; n++) {
        if (dateTrans[n]["date"].getDate() - dateTrans[n - 1]["date"].getDate() > 2) {
            var beginPeriodsOfDays = new Date(dateTrans[n - 1]["date"].getFullYear(), dateTrans[n - 1]["date"].getMonth(), dateTrans[n - 1]["date"].getDate(), 0, 0, 0);
            var endPeriodsOfDays = new Date(dateTrans[n]["date"].getFullYear(), dateTrans[n]["date"].getMonth(), dateTrans[n]["date"].getDate(), 0, 0, 0);
            var skipDays = (endPeriodsOfDays - beginPeriodsOfDays) / (1000 * 3600 * 24);
            var koeff;
            for (v = 0; v < skipDays; v++) {
                koeff = v + 1;
                datesWithWeekends.splice(n, 0, {
                    "Id": dateTrans[n - 1]["Id"],
                    "Name": dateTrans[n - 1]["Name"],
                    "date": new Date(dateTrans[n - 1]["date"].getFullYear(), dateTrans[n - 1]["date"].getMonth(), dateTrans[n - 1]["date"].getDate() + koeff, 0, 0, 0),
                    "rate": dateTrans[n - 1]["rate"]
                });
            }
        } else {
            datesWithWeekends.push(dateTrans[n])
        }
    }

    for (var b = 0; b < totalRates.length; b++) {
        var period;
        var amountMin = totalRates[b]["AmountMin"];
        var amountMax = totalRates[b]["AmountMax"];
        for (var i = datesWithWeekends[0]["date"]; i.getTime() < datesWithWeekends[datesWithWeekends.length - 1]["date"].getTime();) {
            if (totalRates[b]["Period"] == "Week") {
                period = new Date(i.getFullYear(), i.getMonth(), i.getDate() + sevenDays);
            } else if (totalRates[b]["Period"] == "Month") {
                period = new Date(i.getFullYear(), i.getMonth() + 1, i.getDate());
            } else {
                period = new Date(i.getFullYear() + 1, i.getMonth(), i.getDate());
            };
            for (var j = 0; j < totalRates[b]["Rate"]; j++) {
                if (period.getTime() < datesWithWeekends[datesWithWeekends.length - 1]["date"].getTime()) {
                    randDate = new Date(Math.floor(Math.random() * (+period - +i) + +i));
                    var amount = Math.floor(amountMin + (amountMax - amountMin) * Math.random());
                    await transactionsCollection.insertOne({
                        "date": randDate,
                        "category": totalRates[b]["Operation Name"],
                        "amount": amount,
                        "type": totalRates[b]["Type"],
                        "currency": totalRates[b]["Currency"]
                    });
                }

            }
            i = period;
        }
    }

    //task3, task4
    //cashflow + USD
    var cashflowTemp = [];
    var cashflowUsd = [];
    var transactionAsArray = await transactionsCollection.find({}).sort({ date: 1 }).toArray();
    var transactionsWithConvers = [];
    for (var z = 0; z < transactionAsArray.length; z++) {
        if (transactionAsArray[z]["currency"] == "Rub") {
            for (var w = 0; w < datesWithWeekends.length; w++) {
                var taa_str = transactionAsArray[z]["date"].toISOString().split("T")[0];
                var dww_str = datesWithWeekends[w]["date"].toISOString().split("T")[0];
                if (taa_str == dww_str) {
                    transactionsWithConvers.push({
                        "date": transactionAsArray[z]["date"],
                        "category": transactionAsArray[z]["category"],
                        "amount": transactionAsArray[z]["amount"] / datesWithWeekends[w]["rate"],
                        "type": transactionAsArray[z]["type"],
                        "currency": transactionAsArray[z]["currency"]
                    });
                }
            }
        } else {
            transactionsWithConvers.push({
                "date": transactionAsArray[z]["date"],
                "category": transactionAsArray[z]["category"],
                "amount": transactionAsArray[z]["amount"],
                "type": transactionAsArray[z]["type"],
                "currency": transactionAsArray[z]["currency"]
            });
        }
    }

    var j = new Date(datesWithWeekends[0]["date"].getFullYear(), datesWithWeekends[0]["date"].getMonth(), datesWithWeekends[0]["date"].getDate() + sevenDays);
    while (j.getTime() < datesWithWeekends[datesWithWeekends.length - 1]["date"].getTime()) {
        var weekEnd;
        for (var i = 0; i < transactionsWithConvers.length; i++) {
            if (datesWithWeekends[datesWithWeekends.length - 1]["date"].getTime() - j.getTime() < (sevenDays * 24 * 1000 * 3600)) {
                var summ = 0;
                if (transactionsWithConvers[i]["date"].getTime() > j.getTime() && transactionsWithConvers[i]["date"].getTime() <= transactionsWithConvers[transactionsWithConvers.length - 1]["date"].getTime()) {
                    if (transactionsWithConvers[i]["type"] == "Inc") {
                        summ += transactionsWithConvers[i]["amount"];
                    } else {
                        summ -= transactionsWithConvers[i]["amount"];
                    }
                }
                weekEnd = datesWithWeekends[datesWithWeekends.length - 1]["date"];
            }

            if (transactionsWithConvers[i]["date"].getTime() <= j.getTime()) {
                var summ = 0;
                if (transactionsWithConvers[i]["type"] == "Inc") {
                    summ += transactionsWithConvers[i]["amount"];
                } else {
                    summ -= transactionsWithConvers[i]["amount"];
                }
                weekEnd = j;
            }
        }
        cashflowTemp.push({ "everyweek_total_summ": summ, "current_week": weekEnd });
        j = new Date(j.getFullYear(), j.getMonth(), j.getDate() + sevenDays)
    }

    var a = cashflowTemp[0]["everyweek_total_summ"];
    var b;
    var c;
    cashflowTemp.push(cashflowTemp[0]);

    for (var n = 1; n < cashflowTemp.length; n++) {
        b = cashflowTemp[n]["everyweek_total_summ"];
        c = Number((a + b).toFixed(2));
        cashflowUsd.push({ "everyweek_cashflow": c, "current_week": cashflowTemp[n]["current_week"] });
        a = c;
    }

    for (var q = 0; q < cashflowUsd.length; q++) {
        await cashflowUsdCollection.insertOne({
            result: cashflowUsd[q]
        });
    }

}