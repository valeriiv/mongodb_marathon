db.getCollection('cashflow').drop();

var datesArr = [];
var ratesArr = [];
var tempTrans = [];
var cashflowTemp = [];
var cashflowUsd = [];
var sixDaysInMilliseconds = 6*1000*3600*24;
var sevenDaysInMilliseconds = 7*1000*3600*24;

var rate = db.rates.find({}).sort({date: 1}).toArray();
var transactionsWithConvers = db.transactions.find({}).sort({date: 1}).toArray();

for(var i = 0; i < rate.length; i++) {
    datesArr.push(rate[i]["date"]);
    ratesArr.push(rate[i]["rate"]);
    }

loop1:
for (var j = 0; j < transactionsWithConvers.length; j++) {
    if(transactionsWithConvers[j]["currency"] == "Rub") {
    for(var n = 1; n < ratesArr.length; n++) {
        if(transactionsWithConvers[j]["date"].toISOString().substring(0, 10) == rate[n]["date"].toISOString().substring(0, 10)) {
            tempTrans.push({
                    date: transactionsWithConvers[j]["date"],
                    category: transactionsWithConvers[j]["category"],
                    amount: transactionsWithConvers[j]["amount"] / rate[n]["rate"],
                    type: transactionsWithConvers[j]["type"]
            });
            continue loop1;
    } else {
           tempTrans.push({
                    date: transactionsWithConvers[j]["date"],
                    category: transactionsWithConvers[j]["category"],
                    amount: transactionsWithConvers[j]["amount"] / rate[n-1]["rate"],
                    type: transactionsWithConvers[j]["type"]
            });
            
            continue loop1;
        }
        }
    } else {
            tempTrans.push({
                    date: transactionsWithConvers[j]["date"],
                    category: transactionsWithConvers[j]["category"],
                    amount: transactionsWithConvers[j]["amount"],
                    type: transactionsWithConvers[j]["type"]
            });
       }
}

var weekEnd;
for(var d = new Date(tempTrans[0]["date"].getTime() + sevenDaysInMilliseconds); d < tempTrans[tempTrans.length-1]["date"];
 d = new Date(d.getTime() + sevenDaysInMilliseconds)) {  
    var summ = 0;
    for(var p = 0; p < tempTrans.length; p++) {
        if(tempTrans[p]["date"].getTime() < d.getTime() && tempTrans[p]["date"].getTime() > d.getTime() - sixDaysInMilliseconds) {
            if(tempTrans[p]["type"] == "Inc") {
                summ += tempTrans[p]["amount"];    
            } else {
                summ -= tempTrans[p]["amount"];
            }  
        }
            weekEnd = new Date(d.getTime());
    }
    cashflowTemp.push({"date": weekEnd, "cashflow_value": summ});
}

var a = cashflowTemp[0]["cashflow_value"];
var b;
var c;
cashflowUsd.push(cashflowTemp[0]);
       
for (var n = 1; n < cashflowTemp.length; n++) {
    b = cashflowTemp[n]["cashflow_value"];
    c = a + b;
    cashflowUsd.push({"date": cashflowTemp[n]["date"], "cashflow_value": c});
    a = c;
};

for(var po = 0; po < cashflowUsd.length; po++) {
    db.cashflow.insertOne(cashflowUsd[po]);
}