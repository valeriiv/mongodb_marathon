const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');

const url = 'mongodb://localhost:27017';

const dbName = 'mongodbmarathon';

MongoClient.connect(url, function(err, client) {
    assert.equal(null, err);

    const db = client.db(dbName);
    const rates = db.collection('rates');
    const transactions = db.collection('transactions');
    const marathon = db.collection('marathon');
    const cashflowUsd = db.collection('cashflow');
    const fewFriends = db.collection('few_friends');
    const transactionsWithUsd = db.collection('transaction_with_usd');
    const maxDebt = db.collection('maxDebt');
    workWith3Collections(marathon, rates, transactions, cashflowUsd, fewFriends, transactionsWithUsd, maxDebt).then(res => {
        client.close();
    });

});

async function workWith3Collections(marathonCollection, ratesCollection, transactionsCollection, cashflowUsdCollection, fewFriendsCollection,
    transactionsWithUsdCollection, maxDebtCollection) {
    var sevenDaysInMilliseconds = 7 * 1000 * 3600 * 24;
    var sixDaysInMilliseconds = 6 * 1000 * 3600 * 24;
    var sevenDays = 7;
    // var dropTransactions = await transactionsCollection.drop();
    // var dropCashflowUsd = await cashflowUsdCollection.drop();
    var totalRates = await marathonCollection.find({}).toArray();
    var dateTrans = await ratesCollection.find({}).sort({ date: 1 }).toArray();
    var randDate;
    var datesWithWeekends = [];
    datesWithWeekends.push(dateTrans[0]);
    var friendsArr = await fewFriendsCollection.find({}).toArray();
    await fewFriendsCollection.insertMany([{ "name": "Michle", "index": 0, "amount": 0 },
    { "name": "Jane", "index": 1, "amount": 0 },
    { "name": "Bill", "index": 2, "amount": 0 }
    ]);

    for (var n = 1; n < dateTrans.length; n++) {
    if (dateTrans[n]["date"].getDate() - dateTrans[n-1]["date"].getDate() > 2) {
                      var previous = dateTrans[n-1]["date"].getTime();
                      var last = dateTrans[n]["date"].getTime();
                         for(let v = last - 1000*3600*24; v > previous; v = v - 1000*3600*24) {
                            datesWithWeekends.splice(n, 0, {
                              "Id": dateTrans[n-1]["Id"],
                              "Name":  dateTrans[n-1]["Name"],
                              "date" : new Date(v),
                              "rate": dateTrans[n-1]["rate"]
                             });
                         }
                 } else {
                    datesWithWeekends.push(dateTrans[n])
                 }
             }

    for (var b = 0; b < totalRates.length; b++) {
        var period;
        var amountMin = totalRates[b]["AmountMin"];
        var amountMax = totalRates[b]["AmountMax"];
        for (var i = new Date(datesWithWeekends[0]["date"]); i.getTime() < datesWithWeekends[datesWithWeekends.length - 1]["date"].getTime();) {
            if (totalRates[b]["Period"] == "Week") {
                period = new Date(i.getFullYear(), i.getMonth(), i.getDate() + sevenDays);
            } else if (totalRates[b]["Period"] == "Month") {
                period = new Date(i.getFullYear(), i.getMonth() + 1, i.getDate());
            } else {
                period = new Date(i.getFullYear() + 1, i.getMonth(), i.getDate());
            };

            for (var j = 0; j < totalRates[b]["Rate"]; j++) {
                if (period.getTime() < datesWithWeekends[datesWithWeekends.length - 1]["date"].getTime()) {
                    randDate = new Date(Math.floor(Math.random() * (+period - +i) + +i));
                    var amount = Math.floor(amountMin + (amountMax - amountMin) * Math.random());
                    await transactionsCollection.insertOne({
                        "date": randDate,
                        "category": totalRates[b]["Operation Name"],
                        "amount": amount,
                        "type": totalRates[b]["Type"],
                        "currency": totalRates[b]["Currency"]
                    });
                }

            }
            i = period;
        }
    }
     
    //task3, task4
    //cashflow + USD
    var cashflowTemp = [];
    var cashflowUsd = [];
    var transactionAsArray = await transactionsCollection.find({}).sort({date: 1}).toArray();
    var transactionsWithConvers = [];
    for (var z = 0; z < transactionAsArray.length; z++) {
        if (transactionAsArray[z]["currency"] == "Rub") {
            for (var w = 0; w < datesWithWeekends.length; w++) {
                var taa_str = transactionAsArray[z]["date"].toISOString().split("T")[0];
                var dww_str = datesWithWeekends[w]["date"].toISOString().split("T")[0];
                if (taa_str == dww_str) {
                    transactionsWithConvers.push({
                        "date": transactionAsArray[z]["date"],
                        "category": transactionAsArray[z]["category"],
                        "amount": transactionAsArray[z]["amount"] / datesWithWeekends[w]["rate"],
                        "type": transactionAsArray[z]["type"],
                        "currency": transactionAsArray[z]["currency"]
                    });
                }
            }
        } else {
            transactionsWithConvers.push({
                "date": transactionAsArray[z]["date"],
                "category": transactionAsArray[z]["category"],
                "amount": transactionAsArray[z]["amount"],
                "type": transactionAsArray[z]["type"],
                "currency": transactionAsArray[z]["currency"]
            });
        }
    }

var weekEnd;
for(var k = new Date(transactionsWithConvers[0]["date"].getTime() + sevenDaysInMilliseconds); k < transactionsWithConvers[transactionsWithConvers.length-1]["date"];
 k = new Date(k.getTime() + sevenDaysInMilliseconds)) {
    var summ = 0;
    for(var l = 0; l < transactionsWithConvers.length; l++) {
        if(transactionsWithConvers[l]["date"].getTime() < k.getTime() && transactionsWithConvers[l]["date"].getTime() > k.getTime() - sixDaysInMilliseconds) {
            if(transactionsWithConvers[l]["type"] == "Inc") {
                summ += transactionsWithConvers[l]["amount"];    
            } else {
                summ -= transactionsWithConvers[l]["amount"];
            }  
        }
            weekEnd = new Date(k.getTime());
    }
    cashflowTemp.push({"date": weekEnd, "cashflow_value": summ});
}

var a = cashflowTemp[0]["cashflow_value"];
var b;
var c;
cashflowUsd.push(cashflowTemp[0]);

for (var n = 1; n < cashflowTemp.length; n++) {
    b = cashflowTemp[n]["cashflow_value"];
    c = a + b;
    cashflowUsd.push({ "date": cashflowTemp[n]["date"], "cashflow_value": Number(c.toFixed(2)) });
    a = c;
};

for (var po = 0; po < cashflowUsd.length; po++) {
    cashflowUsdCollection.insertOne(cashflowUsd[po]);
}

cashflowWithoutNegativeValues(cashflowTemp, transactionsCollection);
borrow_friends(friendsArr, cashflowTemp, transactionsCollection, fewFriendsCollection);

//task5
transactionsWithConvers.forEach(function(param){
    if(param.type == "Exp") {
       param.amount *= -1;
       }
       transactionsWithUsdCollection.insertOne(param);
       });

//task6
transactionsWithUsdCollection.aggregate([
    {$group: {_id: {category: "$category", year: {$year: "$date"}}, summarizedAmount:  {$sum: "$amount"} } },
    {$group: {_id: "$_id.category", summaryCategories: {$push: "$$ROOT"} } }
    ]);

//task7
var finalDebtArray = await transactionsCollection.find({}).toArray();
var totalSumm = 0;
var maxDebtTemp = [];
for (var k = 0; k < friendsArr.length; k++) {
    for (var l = 0; l < finalDebtArray.length; l++) {
        if(finalDebtArray[l]["name"] == friendsArr[k]["name"]) {
            totalSumm += finalDebtArray[l]["amount"];
            maxDebtTemp.push(
              {"name": friendsArr[k]["name"],
              "amount": totalSumm,
              "date": finalDebtArray[l]["date"],
              })
        } 
    } totalSumm = 0;
}
        maxDebtTemp.forEach(function(vel) {
        maxDebtCollection.insertOne(vel);
        });
}

var cashflowWithoutNegativeValues = function(cashflowTempArray, transactionsCollection){
    var totalDebt = 0;
    var storage = 0;
    for(var w = 0; w < cashflowTempArray.length-1; w++) {
        if (cashflowTempArray[w]["cashflow_value"] + cashflowTempArray[w+1]["cashflow_value"] < 0) {
            if (storage == 0) {
                totalDebt += Math.abs(cashflowTempArray[w]["cashflow_value"]) + 
                Math.abs(cashflowTempArray[w+1]["cashflow_value"]);
                transactionsCollection.insertOne({
                    date: cashflowTempArray[w]["date"],
                    category: "Borrow",
                    amount: Number((Math.abs(cashflowTempArray[w]["cashflow_value"]) + Math.abs(cashflowTempArray[w+1]["cashflow_value"]) ).toFixed(2)),
                    type: "Inc",
                    currency: "Usd"
                });
            }
            else if (storage > 0 && storage < Math.abs(cashflowTempArray[w+1]["cashflow_value"]) + 
            Math.abs(cashflowTempArray[w+1]["cashflow_value"])) {
                totalDebt = totalDebt + Math.abs(cashflowTempArray[w]["cashflow_value"]) + 
                Math.abs(cashflowTempArray[w+1]["cashflow_value"]) - storage;
                storage = 0;
                transactionsCollection.insertOne({
                    date: cashflowTempArray[w]["date"],
                    category: "Borrow",
                    amount: Number((Math.abs(cashflowTempArray[w]["cashflow_value"]) + 
                    Math.abs(cashflowTempArray[w+1]["cashflow_value"]) - storage).toFixed(2)),
                    type: "Inc",
                    currency: "Usd"
                });
            }
            else if (storage >= cashflowTempArray[w]["cashflow_value"] + cashflowTempArray[w+1]["cashflow_value"] 
            && storage >= totalDebt && totalDebt > 0) {
                totalDebt = totalDebt + Math.abs(cashflowTempArray[w]["cashflow_value"]) + 
                Math.abs(cashflowTempArray[w+1]["cashflow_value"]) - storage;
                storage = 0;
                transactionsCollection.insertOne({
                    date: cashflowTempArray[w]["date"],
                    category: "Borrow",
                    amount: Number(Math.abs(cashflowTempArray[w]["cashflow_value"]).toFixed(2)),
                    type: "Exp",
                    currency: "Usd"
                });
                storage -= Number((Math.abs(cashflowTempArray[w]["cashflow_value"])).toFixed(2));
                totalDebt = 0;
            }
        } else {
            storage += Number((cashflowTempArray[w]["cashflow_value"] + cashflowTempArray[w+1]["cashflow_value"]).toFixed(2));
        }
    }
    return cashflowTempArray;
};

var borrow_friends = function(friendsAsArray, cashflowTempAsArray, transactionsCollection, fewFriendsCollection) {
    var totalDebt = 0;
    var storage = 0;
    for (var jj = 0; jj < cashflowTempAsArray.length-1; jj++) {
        var randomFriend = friendsAsArray[Math.floor(Math.random() * 3)];
        if (cashflowTempAsArray[jj]["cashflow_value"] + cashflowTempAsArray[jj+1]["cashflow_value"] < 0) {
            if (storage == 0) {
                totalDebt += Math.abs(cashflowTempAsArray[jj]["cashflow_value"]) + Math.abs(cashflowTempAsArray[jj+1]["cashflow_value"]); 
                fewFriendsCollection.update({ "name": randomFriend["name"] }, {
                    $set: { "amount": randomFriend["amount"] - Number(((Math.abs(cashflowTempAsArray[jj]["cashflow_value"]) + 
                    Math.abs(cashflowTempAsArray[jj+1]["cashflow_value"]))).toFixed(2)) }
                });
                transactionsCollection.insertOne(
                    {"name": randomFriend["name"],
                    "amount": Number((Math.abs(cashflowTempAsArray[jj]["cashflow_value"]) + 
                    Math.abs(cashflowTempAsArray[jj+1]["cashflow_value"])).toFixed(2)),
                    "date": cashflowTempAsArray[jj]["date"],
                    "type": "Exp"
                });
            } else if (storage > 0 && storage < Math.abs(cashflowTempAsArray[jj]["cashflow_value"]) + 
            Math.abs(cashflowTempAsArray[jj+1]["cashflow_value"]) ) {
                    totalDebt = totalDebt + Math.abs(cashflowTempAsArray[jj]["cashflow_value"]) + 
                    Math.abs(cashflowTempAsArray[jj+1]["cashflow_value"]) - storage;
                    storage = 0;
                fewFriendsCollection.update({ "name": randomFriend["name"] }, {
                    $set: { "amount": Number((Math.abs(cashflowTempAsArray[jj]["cashflow_value"]) + 
                    Math.abs(cashflowTempAsArray[jj+1]["cashflow_value"]) - storage).toFixed(2)) }
                });
                transactionsCollection.insertOne(
                    {"name": randomFriend["name"],
                    "amount": Number((Math.abs(cashflowTempAsArray[jj]["cashflow_value"]) + 
                    Math.abs(cashflowTempAsArray[jj+1]["cashflow_value"]) - storage).toFixed(2)),
                    "date": cashflowTempAsArray[jj]["date"],
                    "type": "Exp"
                });
            } else if (storage >= totalDebt && storage > 0 ) {
                fewFriendsCollection.update({ "name": randomFriend["name"] }, {
                    $set: { "amount": (randomFriend["amount"] + Number((cashflowTempAsArray[jj]["cashflow_value"]).toFixed(2)) ) }
                });
                transactionsCollection.insertOne(
                    {"name": randomFriend["name"],
                    "amount": Number((Math.abs(cashflowTempAsArray[jj]["cashflow_value"])).toFixed(2)),
                    "date": cashflowTempAsArray[jj]["date"],
                    "type": "Inc"
                  });
                  storage -= Number((Math.abs(cashflowTempAsArray[jj]["cashflow_value"])).toFixed(2));
                  totalDebt -= Number((Math.abs(cashflowTempAsArray[jj]["cashflow_value"])).toFixed(2));
            }
        } else {
            storage += Number((cashflowTempAsArray[jj]["cashflow_value"] + 
            cashflowTempAsArray[jj+1]["cashflow_value"]).toFixed(2));
        } 
    }
};