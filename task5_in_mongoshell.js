db.getCollection('cashflow').drop();
db.getCollection('few_friends').drop();
db.getCollection('friends_debt_statistic').drop();
db.createCollection('few_friends');
db.createCollection("friends_debt_statistic");
db.getCollection('few_friends').insertMany([{ "name": "Michle", "index": 0, "amount": 0 },
    { "name": "Jane", "index": 1, "amount": 0 },
    { "name": "Bill", "index": 2, "amount": 0 }
]);

var cashflowWithoutNegativeValues = function(cashflowTempArray){
    var totalDebt = 0;
    var storage = 0;
    for(var w = 0; w < cashflowTempArray.length-1; w++) {
        if (cashflowTempArray[w]["cashflow_value"] + cashflowTempArray[w+1]["cashflow_value"] < 0) {
            if (storage == 0) {
                totalDebt += Math.abs(cashflowTempArray[w]["cashflow_value"]) + 
                Math.abs(cashflowTempArray[w+1]["cashflow_value"]);
                db.transactions.insertOne({
                    date: cashflowTempArray[w]["date"],
                    category: "Borrow",
                    amount: Number((Math.abs(cashflowTempArray[w]["cashflow_value"]) + Math.abs(cashflowTempArray[w+1]["cashflow_value"]) ).toFixed(2)),
                    type: "Inc",
                    currency: "Usd"
                });
            }
            else if (storage > 0 && storage < Math.abs(cashflowTempArray[w+1]["cashflow_value"]) + 
            Math.abs(cashflowTempArray[w+1]["cashflow_value"])) {
                totalDebt = totalDebt + Math.abs(cashflowTempArray[w]["cashflow_value"]) + 
                Math.abs(cashflowTempArray[w+1]["cashflow_value"]) - storage;
                storage = 0;
                db.transactions.insertOne({
                    date: cashflowTempArray[w]["date"],
                    category: "Borrow",
                    amount: Number((Math.abs(cashflowTempArray[w]["cashflow_value"]) + 
                    Math.abs(cashflowTempArray[w+1]["cashflow_value"]) - storage).toFixed(2)),
                    type: "Inc",
                    currency: "Usd"
                });
            }
            else if (storage >= cashflowTempArray[w]["cashflow_value"] + cashflowTempArray[w+1]["cashflow_value"] 
            && storage >= totalDebt && totalDebt > 0) {
                totalDebt = totalDebt + Math.abs(cashflowTempArray[w]["cashflow_value"]) + 
                Math.abs(cashflowTempArray[w+1]["cashflow_value"]) - storage;
                storage = 0;
                db.transactions.insertOne({
                    date: cashflowTempArray[w]["date"],
                    category: "Borrow",
                    amount: Number(Math.abs(cashflowTempArray[w]["cashflow_value"]).toFixed(2)),
                    type: "Exp",
                    currency: "Usd"
                });
                storage -= Number((Math.abs(cashflowTempArray[w]["cashflow_value"])).toFixed(2));
                totalDebt = 0;
            }
        } else {
            storage += Number((cashflowTempArray[w]["cashflow_value"] + cashflowTempArray[w+1]["cashflow_value"]).toFixed(2));
        }
    }
    return cashflowTempArray;
};

var borrow_friends = function(friendsAsArray, cashflowTempAsArray) {
    var totalDebt = 0;
    var storage = 0;
    for (var jj = 0; jj < cashflowTempAsArray.length-1; jj++) {
        var randomFriend = friendArr[Math.floor(Math.random() * 3)];
        if (cashflowTempAsArray[jj]["cashflow_value"] + cashflowTempAsArray[jj+1]["cashflow_value"] < 0) {
            if (storage == 0) {
                totalDebt += Math.abs(cashflowTempAsArray[jj]["cashflow_value"]) + Math.abs(cashflowTempAsArray[jj+1]["cashflow_value"]); 
                db.few_friends.update({ "name": randomFriend["name"] }, {
                    $set: { "amount": randomFriend["amount"] - Number(((Math.abs(cashflowTempAsArray[jj]["cashflow_value"]) + 
                    Math.abs(cashflowTempAsArray[jj+1]["cashflow_value"]))).toFixed(2)) }
                });
                db.friends_debt_statistic.insertOne(
                    {"name": randomFriend["name"],
                    "amount": Number((Math.abs(cashflowTempAsArray[jj]["cashflow_value"]) + 
                    Math.abs(cashflowTempAsArray[jj+1]["cashflow_value"])).toFixed(2)),
                    "date": cashflowTempAsArray[jj]["date"],
                    "type": "Exp"
                });
            } else if (storage > 0 && storage < Math.abs(cashflowTempAsArray[jj]["cashflow_value"]) + 
            Math.abs(cashflowTempAsArray[jj+1]["cashflow_value"]) ) {
                    totalDebt = totalDebt + Math.abs(cashflowTempAsArray[jj]["cashflow_value"]) + 
                    Math.abs(cashflowTempAsArray[jj+1]["cashflow_value"]) - storage;
                    storage = 0;
                db.few_friends.update({ "name": randomFriend["name"] }, {
                    $set: { "amount": Number((Math.abs(cashflowTempAsArray[jj]["cashflow_value"]) + 
                    Math.abs(cashflowTempAsArray[jj+1]["cashflow_value"]) - storage).toFixed(2)) }
                });
                db.friends_debt_statistic.insertOne(
                    {"name": randomFriend["name"],
                    "amount": Number((Math.abs(cashflowTempAsArray[jj]["cashflow_value"]) + 
                    Math.abs(cashflowTempAsArray[jj+1]["cashflow_value"]) - storage).toFixed(2)),
                    "date": cashflowTempAsArray[jj]["date"],
                    "type": "Exp"
                });
            } else if (storage >= totalDebt && storage > 0 ) {
                db.few_friends.update({ "name": randomFriend["name"] }, {
                    $set: { "amount": (randomFriend["amount"] + Number((cashflowTempAsArray[jj]["cashflow_value"]).toFixed(2)) ) }
                });
                db.friends_debt_statistic.insertOne(
                    {"name": randomFriend["name"],
                    "amount": Number((Math.abs(cashflowTempAsArray[jj]["cashflow_value"])).toFixed(2)),
                    "date": cashflowTempAsArray[jj]["date"],
                    "type": "Inc"
                  });
                  storage -= Number((Math.abs(cashflowTempAsArray[jj]["cashflow_value"])).toFixed(2));
                  totalDebt -= Number((Math.abs(cashflowTempAsArray[jj]["cashflow_value"])).toFixed(2));
            }
        } else {
            storage += Number((cashflowTempAsArray[jj]["cashflow_value"] + 
            cashflowTempAsArray[jj+1]["cashflow_value"]).toFixed(2));
        } 
    }
};

var datesArr = [];
var ratesArr = [];
var tempTrans = [];
var cashflowTemp = [];
var cashflowUsd = [];
var friendArr = db.few_friends.find({}).toArray();
var sevenDaysInMilliseconds = 7 * 1000 * 3600 * 24;
var sixDaysInMilliseconds = 6 * 1000 * 3600 * 24;

var rate = db.rates.find({}).sort({ date: 1 }).toArray();
var transactionsWithConvers = db.transactions.find({}).sort({ date: 1 }).toArray();

for (var i = 0; i < rate.length; i++) {
    datesArr.push(rate[i]["date"]);
    ratesArr.push(rate[i]["rate"]);
}

loop1:
    for (var j = 0; j < transactionsWithConvers.length; j++) {
        if (transactionsWithConvers[j]["currency"] == "Rub") {
            for (var n = 1; n < rate.length; n++) {
                if (transactionsWithConvers[j]["date"].toISOString().substring(0, 10) == rate[n]["date"].toISOString().substring(0, 10)) {
                    tempTrans.push({
                        date: transactionsWithConvers[j]["date"],
                        category: transactionsWithConvers[j]["category"],
                        amount: transactionsWithConvers[j]["amount"] / rate[n]["rate"],
                        type: transactionsWithConvers[j]["type"]
                    });
                    continue loop1;
                } else {
                    tempTrans.push({
                        date: transactionsWithConvers[j]["date"],
                        category: transactionsWithConvers[j]["category"],
                        amount: transactionsWithConvers[j]["amount"] / rate[n - 1]["rate"],
                        type: transactionsWithConvers[j]["type"]
                    });

                    continue loop1;
                }
            }
        } else {
            tempTrans.push({
                date: transactionsWithConvers[j]["date"],
                category: transactionsWithConvers[j]["category"],
                amount: transactionsWithConvers[j]["amount"],
                type: transactionsWithConvers[j]["type"]
            });
        }
    }

var weekEnd;
for (var d = new Date(tempTrans[0]["date"].getTime() + sevenDaysInMilliseconds); d < tempTrans[tempTrans.length - 1]["date"]; d = new Date(d.getTime() + sevenDaysInMilliseconds)) {
    var summ = 0;
    for (var p = 0; p < tempTrans.length; p++) {
        if (tempTrans[p]["date"].getTime() <= d.getTime() && tempTrans[p]["date"].getTime() >= d.getTime() - sixDaysInMilliseconds) {
            if (tempTrans[p]["type"] == "Inc") {
                summ += tempTrans[p]["amount"];
            } else {
                summ -= tempTrans[p]["amount"];
            }
            weekEnd = new Date(d.getTime());
        }
    }
        cashflowTemp.push({ date: weekEnd, "cashflow_value": summ });
        summ = 0;
}

var a = cashflowTemp[0]["cashflow_value"];
var b;
var c;
cashflowUsd.push(cashflowTemp[0]);

for (var n = 1; n < cashflowTemp.length; n++) {
    b = cashflowTemp[n]["cashflow_value"];
    c = a + b;
    cashflowUsd.push({ "date": cashflowTemp[n]["date"], "cashflow_value": Number(c.toFixed(2)) });
    a = c;
};

for (var po = 0; po < cashflowUsd.length; po++) {
    db.cashflow.insertOne(cashflowUsd[po]);
}

cashflowWithoutNegativeValues(cashflowTemp);
borrow_friends(friendArr, cashflowTemp);